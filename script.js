// console.log("Good Day!");
/* 
    ES6 Updates
    ES6 is one of the latest versions of writing Javascript and in fact is one of the latest major update to JS.

    let,const - are ES6 updates to update the standard of creating variables.
    var - was the keyword used previously before ES6

*/

// console.log(sampleLet);
// let sampleLet = "Sample";

console.log(varSample);
var varSample = "Hoist Me Up!";

let fivePowerOf3 = Math.pow(5,3);
console.log(fivePowerOf3);
//Math.pow() allows us to get the result of number raised to the given exponent
//Math.pow(base,exponent)

//Exponent Operators - ** - allows us to get the result of number raised to a given exponent. It is used as an alternative to Math.pow();

let fivePowerof2 = 5**2;
console.log(fivePowerof2);

//Can we also get the square root of a number with exponent operator?
let squareRootOf4 = 4**.5;
console.log(squareRootOf4);

//Template literals
// "",'' = string literals

//How do we combile strings?
let word1 = "Javascript";
let word2 = "Java";
let word3 = "is";
let word4 = "not";

console.log("Result from using the traditional way + to concatinating a variable strings:")
let sentence1 = word1 + " " + word3+ " " + word4 + " " + word2;
console.log(sentence1);

//`` -> backticks - Template Literals - allows us to create strings using `` and easily embed JS expressions.
//${} is used in template literals to embed JS expressions and variables.
// ${} - placeholder
console.log("Result from using backticks `` :")
let sentence =`${word1} ${word3} ${word4} ${word2}`;
console.log(sentence)
let sentence2 = `${word2} is an OOP Language.`
console.log(sentence2);

//Template Literals can also be used to embed JS expressions
let sentence3 = `The sum of 15 and 25 is ${15+25}`;
console.log(sentence3);

let user1 = {
    name: "Michael",
    position: "Manager",
    income: 90000,
    expenses: 50000
}

console.log(`${user1.name} is a ${user1.position}`);
console.log(`His income ${user1.income} and expenses at ${user1.expenses}. His current balance is ${user1.income - user1.expenses}`);

//Destructuring Arrays and Objects
//Destructuring will allow us to save Array elements or Object properties into new variables without having to create/initialize with accessing items/properties one by one
let array1 = ["Curry","Lillard","Paul","Irving"];
/* 
let player1 = array1[0];//Curry
let player2 = array1[1];
let player3 = array1[2];
let player4 = array1[3];
console.log("Result of normal way of accessing every single index in an array:");
console.log(player1,player2,player3,player4); */

//Array Destructuring is when we save array items into variable.
//In arrays, order matters and that goes the same for destruturing:
let [player1,player2,player3,player4] = array1;

console.log(player1);
console.log(player4);

let array2 = ["Jokic","Embiid","Anthony-Towns","Davis"];

let[center1,,,center2]= array2; // the way to skip an array index to get the last index we used , just like in the example
console.log(center1);
console.log(center2);

//Object Destructuring
//Object Destructuring allows us to get the value of a property and save in a variable of the same name.

let pokemon1 = {
    name: "Bulbasaur",
    type: "Grass",
    level: 10,
    moves: ["Razor leaf","Tackle","Leech Seed"]
}
// Order does not matter in destructuring Object.
//What matters are the keys/property name.
//The variable name should exactly match the property name.
let {type,level,name,movesies,personality} = pokemon1;
console.log(type);
console.log(level);
console.log(movesies); // undefined
console.log(name);
console.log(personality);// undefined

//Destructuring in a function
function greet(object){

    /* 
        //When passed user1, object now contains the key-value pairs of user1
        object ={
            let user1 = {
            name: "Michael",
            position: "Manager",
            income: 90000,
            expenses: 50000
        }
    
    
    */
    let {name} = object;// allowed us to get object.name and save it as name
    let {position} = object; // you can also access other property by initializing it separatetly or declare it like destructuring let {name,position} = object;

     /* 
        if no destructuring:
        console.log(`Hello! ${object.name}`);
        console.log(`${object.name} is my friend!`);
        console.log(`Good Luck, ${object.name}!`); */   

    //With destructuring
    console.log(`Hello! ${name}`);
    console.log(`${name} is my friend!`);
    console.log(`Good Luck, ${name}!`);
    console.log(`${position}`);
}

greet(user1);

//Mini-Activity:
//Destructure the following object and log the values of the name and price in the console:

let produc1 ={

    productName: "Safeguard Handsoap",
    description: "Liquid Handsoap by Safeguard.",
    price: 25,
    isActive: true
}

//Display the name and price in the console by destructuring.

let {productName,price} = produc1;

console.log(`Product: ${productName} Price: ${price}`);

// using constructor function 
/* function displayPriceAndName(product){
    
    let {productName,price} = produc1;

    console.log(`Product: ${productName} Price: ${price}`);
}

displayPriceAndName(produc1); */

//Arrow FUnctions
// Arrow Functions are an alternative way of writing functions in JS.
//However, there significant pros and cons between traditional and arrow functions

//traditional function
function displayMsg(){
    console.log(`Hello, World!`);
}
displayMsg();

// Arrow Function
// we used "=>" symbols to signify that this is a arrow function
//Syntax --> const varArrowFunction = (parameter) => {statement}
const hello = () => {
    console.log(`Hello, Arrow!`);
}

hello();

//Arrow functions with parameters
// We don't usually use let keyword to assign our arrow function to avoid updating the variable
const alertUser = (username) =>{
    console.log(`This is an alert for user ${username}`)
}
alertUser("James1191");

/* let alertUser = (username) =>{
    console.log(`This is an alert for user ${username}`)
}
alertUser("James1191");

alertUser = "Hello, Pogi sir Rome";

alertUser("romenick100"); */

//Arraw and Traditional functions are pretty much the same. They are functions.However, there are some key differences.

//Implicit return - is the ability of an arrow function to return value without the use of return keyword.

//traditional addNum() function

function addNum(num1,num2){
    let result = num1 + num2;
    return result; // traditional function need return keyword to return the value within the function
}

let sum1 =addNum(5,10);
console.log("Result of using traditional function:");
console.log(sum1);

//Arrow Function have implicit return. When an arrow function is written in online, it can return value without return keyword:

const addNumArrow = (num1,num2) => num1+num2;

let sum2 = addNumArrow(10,20);
console.log("Result of using arrow function:");
console.log(sum2);

//Implicit Return will only work on arrow functions written in one line and without {}
//If an arrow function is written in more than one line and with a {} then, we will need a return keyword:

/* const subNum = (num1,num2) => {
    return num1-num2;
} */
const subNum = (num1,num2) => num1 - num2;
let difference = subNum(20,10);
console.log(difference);

//Traditional Functions vs Arrow Functions as Object Methods

let character1 = {
    name: "Cloud Strife",
    occupation: "SOLDIER",
    introduceName: function(){
        console.log(`Hi! I'm ${this.name}`);
    },
    introduceJob: () =>{
        //In an arrow function as method,
        //This acctually refers to the global window object/the whole document.
        
        // console.log(`My job is ${this.occupation}`);
        //This is why it is not advisable to use arrow functions as method.
        console.log(this)
    }
}

character1.introduceName();
character1.introduceJob();

const sampleObj = {
    name: "Sample1",
    age: 25
}

sampleObj.name = "Smith";
console.log(sampleObj)


// Class Based Object Blueprints
    //In Javascript, Classes are templates of Objects
    //We can use classes to create objects following the structure of the class similiar to a constructor function.

    //Constructor Function
    // function Pokemon(name,type,level){
    //     this.name = name;
    //     this.type = type;
    //     this.level = level;
    // }

    // let pokemonInstance1 = new Pokemon("Pikachu","Electric",25);
    // console.log(pokemonInstance1);

    //With the advent of ES6, we are now introduced to a new way of creating objects with a blueprint with the use of Classes.

    class Car {
        constructor(brand,model,year){
            this.brand = brand;
            this.model = model;
            this.year = year;
        }
    }

    let car1 = new Car("Toyota","Vios","2002");
    let car2 = new Car("Cooper","Mini","1997");
    let car3 = new Car("Porsche", "911", "1968");

    console.log(car1);
    console.log(car2);
    console.log(car3);

    // Mini-Activity
    // Translate the Pokemon constructor function into a Class Constructor
    //Create 2 new pokemons out of the class constructor and save it in their variables 
    //Log the values in the console
    //Share your output in the hangouts
     /* let pokemon1 = {
        name: "Bulbasaur",
        type: "Grass",
        level: 10,
        moves: ["Razor leaf","Tackle","Leech Seed"]
    } */
    class Pokemon {
        constructor(name,type,level,moves){
            this.name = name;
            this.type = type;
            this.level = level;
            this.moves = moves;
        }
    }
   let Charizard = new Pokemon("Charizard", "Fire,Flying", 55, ["Flame Thrower","Fly"]);
   console.log(Charizard);

   let Geodude = new Pokemon("Geodude", "Rock, Ground", 15, ["Roll","Dig","Tackle","Magnitude"]);
   console.log(Geodude);


   //Arrow Functions in Array Methods
   let numArr = [2,10,3,10,5];

   //Array Method with Traditional Function
   /* let reduceNumber = numArr.reduce(function(x,y){
        // Get the sum of all numbers in the array
        return x+y;
   }) */
   let reduceNumber = numArr.reduce((x,y)=> {
        return x+y;
   })
   console.log(reduceNumber);

   // Reduce with implicit return
   let reduceNum1 = numArr.reduce((x,y) => x+y)
   console.log(reduceNum1);

   //Tip: If you are still getting confused on making arrow functions with array methods, first create the method with a traditional function, then translatethat into arrow:
   
   let mappedNum = numArr.map((num) =>{
        return num * 2;
   })
   console.log(mappedNum);